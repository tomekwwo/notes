## This application features: ##

* Creating user accounts and logging
* Creating / Reading / Updating / Deleting notes visible only for owner
* Data encryption


Encryption algorithm was found in the Intenet and was only implemented by me. 

It is slow but I added it only for educational purposes.

Application interface is only in polish language.

## Technical details ##

Application is split into few source files:

* DataManager.cs - singleton that is used for saving (encrypting) and loading (decrypting) user and notes data
* Encryption.cs - encryption algorithms used by DataManager
* Input.cs - non standard keyboard input made by me
* Menu.cs - convenient menus that are controlled with arrow keys and Enter key
* Note.cs and User.cs - models of data held in files with basic methods
* Program.cs - main program file which handles user interaction