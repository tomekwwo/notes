﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace Notes
{
    class DataManager
    {
        public static DataManager instance = new DataManager();
        public List<User> users;
        public List<Note> notes;

        private string usersFileName = "UsersData.dat";
        private string notesFileName = "NotesData.dat";
        private string encryptionPass = "3AW%oFXmE3dE$SoE$fxfcT3w$uSdGIm3";

        private DataManager()
        {
            if (instance == null)
                instance = this;
            if (instance != this) return;
            
            LoadUsers();
        }

        public void LoadUsers()
        {
            if (File.Exists(usersFileName))
            {
                Console.Clear();
                Console.Write("\n\tWczytywanie 0%");
                float progress = 0.0f;

                BinaryFormatter bf = new BinaryFormatter();
                FileStream fs = new FileStream(usersFileName, FileMode.Open);
                users = (List<User>) bf.Deserialize(fs);
                fs.Close();

                Progress(++progress, (users.Count * 2) + 1, "\n\tWczytywanie ");

                // decrypt users
                DateTime start = DateTime.Now;
                foreach (User user in users)
                {
                    user.username = Encryption.SimpleDecryptWithPassword(user.username, encryptionPass);
                    Progress(++progress, (users.Count * 2) + 1, "\n\tWczytywanie ");
                    user.password = Encryption.SimpleDecryptWithPassword(user.password, encryptionPass);
                    Progress(++progress, (users.Count * 2) + 1, "\n\tWczytywanie ");
                }
                DateTime finish = DateTime.Now;
                Console.WriteLine("\nUsers decrypted in " + finish.Subtract(start).TotalSeconds + "sec");
                Console.ReadKey();
            }
            else
            {
                users = new List<User>();
            }
        }

        public void SaveUsers()
        {
            Console.WriteLine("\n\tZapisywanie 0%");
            float progress = 0.0f;

            // encrypt users
            DateTime start = DateTime.Now;
            foreach (User user in users)
            {
                user.username = Encryption.SimpleEncryptWithPassword(user.username, encryptionPass);
                Progress(++progress, (users.Count * 2) + 1, "\n\tZapisywanie ");
                user.password = Encryption.SimpleEncryptWithPassword(user.password, encryptionPass);
                Progress(++progress, (users.Count * 2) + 1, "\n\tZapisywanie ");
            }
            DateTime finish = DateTime.Now;

            BinaryFormatter bf = new BinaryFormatter();
            FileStream fs = File.Open(usersFileName, FileMode.OpenOrCreate);
            bf.Serialize(fs, users);
            fs.Close();
            Progress(++progress, (users.Count / 2) + 1, "\n\tZapisywanie ");
            Console.WriteLine("\nUsers encrypted in " + finish.Subtract(start).TotalSeconds + "sec");
            Console.ReadKey();
        }
        
        public void LoadNotes(User user)
        {
            if (File.Exists("Notes/" + user.username + notesFileName))
            {
                Console.WriteLine("\n\tWczytywanie 0%");
                float progress = 0.0f;
                BinaryFormatter bf = new BinaryFormatter();
                FileStream fs = new FileStream("Notes/" + user.username + notesFileName, FileMode.Open);
                notes = (List<Note>) bf.Deserialize(fs);
                fs.Close();
                Progress(++progress, (notes.Count * 3) + 1, "\n\tWczytywanie ");

                // decrypt notes
                DateTime start = DateTime.Now;
                foreach (Note note in notes)
                {
                    note.title = Encryption.SimpleDecryptWithPassword(note.title, user.password);
                    Progress(++progress, (notes.Count * 3) + 1, "\n\tWczytywanie ");
                    note.creator = Encryption.SimpleDecryptWithPassword(note.creator, user.password);
                    Progress(++progress, (notes.Count * 3) + 1, "\n\tWczytywanie ");
                    note.text = Encryption.SimpleDecryptWithPassword(note.text, user.password);
                    Progress(++progress, (notes.Count * 3) + 1, "\n\tWczytywanie ");
                }
                DateTime finish = DateTime.Now;
                Console.WriteLine("\nNotes decrypted in " + finish.Subtract(start).TotalSeconds + "sec");
                Console.ReadKey();
            }
            else
                notes = new List<Note>();
        }

        public void SaveNotes(User user)
        {
            Console.WriteLine("\n\tZapisywanie 0%");
            float progress = 0.0f;

            // clone notes list
            List<Note> encrypt = new List<Note>();
            for (int i = 0; i < notes.Count; i++)
            {
                Note note = new Note();
                note.creator = notes[i].creator;
                note.date = notes[i].date;
                note.title = notes[i].title;
                note.text = notes[i].text;
                encrypt.Add(note);
            }
            Progress(++progress, (notes.Count * 3) + 1, "\n\tZapisywanie ");

            DateTime start = DateTime.Now;
            // encrypt notes
            foreach (Note note in encrypt)
            {
                note.title = Encryption.SimpleEncryptWithPassword(note.title, user.password);
                Progress(++progress, (notes.Count * 3) + 1, "\n\tZapisywanie ");
                note.creator = Encryption.SimpleEncryptWithPassword(note.creator, user.password);
                Progress(++progress, (notes.Count * 3) + 1, "\n\tZapisywanie ");
                note.text = Encryption.SimpleEncryptWithPassword(note.text, user.password);
                Progress(++progress, (notes.Count * 3) + 1, "\n\tZapisywanie ");
            }
            DateTime finish = DateTime.Now;
            Console.WriteLine("\nNotes encrypted in " + finish.Subtract(start).TotalSeconds + "sec");
            Console.ReadKey(true);

            BinaryFormatter bf = new BinaryFormatter();
            FileStream fs = File.Open("Notes/" + user.username + notesFileName, FileMode.OpenOrCreate);
            bf.Serialize(fs, encrypt);
            fs.Close();
            Progress(++progress, (notes.Count * 3) + 1, "\n\tZapisywanie ");
        }

        public bool UserExists(string username)
        {
            foreach (User user in users)
            {
                if (user.username == username)
                    return true;
            }

            return false;
        }

        public User FindUser(string username)
        {
            foreach (User user in users)
            {
                if (user.username == username)
                    return user;
            }

            return null;
        }

        private void Progress(float state, float max, string label)
        {
            Console.Clear();
            Console.Write(label + (int) ((state / max) * 100) + "%");
        }
    }
}
