﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Notes
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.CursorVisible = false;
            User user = new User();
            int input;
            Menu menu;

            LogScreen:
            // log in / register
            while (!user.logged)
            {
                menu = new Menu("--- Zaloguj się ---\n", "\n\tCreated by Tomekwwo");
                menu.AddOption("Logowanie");
                menu.AddOption("Rejestracja");
                menu.AddOption("Wyjdź z aplikacji");
                menu.ShowMenu(0, out input);

                switch (input)
                {
                    case 0:
                        user.LogIn();
                        break;

                    case 1:
                        user.Register();
                        break;

                    case 2:
                        Console.CursorVisible = true;
                        Environment.Exit(0);
                        break;
                }
            }

            DataManager.instance.LoadNotes(user);

            // main loop
            while (true)
            {
                MainMenu:
                menu = new Menu("--- Menu główne ---\t" + user.username + "\n", null);
                menu.AddOption("Stwórz notatkę");
                menu.AddOption("Wyświetl notatki");
                menu.AddOption("Wyloguj się");
                menu.ShowMenu(0, out input);

                switch (input)
                {
                    case 0:
                        user.CreateNote();
                        break;

                    case 1:
                        ViewNotes:
                        Console.Clear();

                        List<Note> userNotes = user.GetNotes();
                        if (userNotes.Count == 0)
                        {
                            Console.WriteLine("Brak notatek!");
                            Console.ReadKey();
                            break;
                        }

                        // take max of 10 notes per page
                        int pages;
                        if (userNotes.Count % 10 == 0)
                        {
                            pages = userNotes.Count / 10;
                        }
                        else
                            pages = (userNotes.Count / 10) + 1;

                        int notesLeft = userNotes.Count;
                        
                        menu = new Menu("--- Notatki ---\n", null, pages, 10);
                        for (int i = 0; i < pages; i++)
                        {
                            int notesToView = 0;
                            if (menu.itemsPerPage <= notesLeft)
                                notesToView = 10;
                            else
                                notesToView = notesLeft;
                            
                            List<Note> notes = userNotes.GetRange(i * menu.itemsPerPage, notesToView);

                            for (int j = 0; j < notesToView; j++)
                            {
                                string title = notes[j].title + " (" + notes[j].date + ")";
                                menu.AddOption(title, i + 1);
                            }
                            notesLeft -= menu.itemsPerPage;
                        }
                        int page = menu.ShowMenuWithPages(0, out input);

                        if (input == -1)
                        {
                            goto MainMenu;
                        }
                        // view chosen note
                        Note view = userNotes[(page - 1) * menu.itemsPerPage + input];

                        menu = new Menu(view.View(), "");
                        menu.AddOption("Edytuj");
                        menu.AddOption("Usuń");
                        menu.AddOption("Wróć");
                        menu.ShowMenu(0, out input);

                        switch(input)
                        {
                            // on escape key -> go back
                            case -1:
                                goto ViewNotes;

                            // edit note
                            case 0:
                                view.Edit();
                                goto ViewNotes;

                            // delete note
                            case 1:
                                menu = new Menu("Usunąć notatkę?", null);
                                menu.AddOption("Tak");
                                menu.AddOption("Nie");
                                menu.ShowMenu(1, out input);

                                if (input == 0)
                                {
                                    DataManager.instance.notes.Remove(view);
                                    DataManager.instance.SaveNotes(user);
                                    Console.Clear();
                                    Console.WriteLine("Notatka usunięta");
                                    Console.ReadKey();
                                    goto ViewNotes;
                                }
                                else
                                {
                                    goto ViewNotes;
                                }

                            // go back
                            case 2:
                                goto ViewNotes;
                        }

                        break;

                    case 2:
                        user.LogOut();
                        goto LogScreen;
                }
            }
        }
    }
}
