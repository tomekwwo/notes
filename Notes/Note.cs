﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Notes
{
    [Serializable]
    class Note
    {
        public DateTime date;
        public string title;
        public string text;
        public string creator;

        public string View()
        {
            string note = "--- " + title + " ---\n";
            note += "Utworzona " + date + "\n\n";
            note += text + "\n";
            return note;
        }

        public void Edit()
        {
            Console.Clear();
            Note edit = DataManager.instance.notes.Find(
                n => n.title == title && n.text == text && n.creator == creator && n.date == date);

            Console.Write("Tytuł: " + title);
            string t = Input.TextLine(1, 50, title, false);

            if (t == null)
                return;

            Console.Clear();
            Console.WriteLine("--- Edytuj ---");
            Console.WriteLine("[ESC], aby anulować, [F12], aby zapisać\n");
            Console.Write(text);
            string t2 = Input.Text(0, 500, text);

            if (t2 == null)
                return;

            edit.title = t;
            edit.text = t2;
            User user = DataManager.instance.users.Find(u => u.username == edit.creator);
            DataManager.instance.SaveNotes(user);
            Console.Clear();
            Console.WriteLine("Notatka zapisana");
            Console.ReadKey();
        }
    }
}
