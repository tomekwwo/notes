﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Notes
{
    class Input
    {
        public static string TextLine(int minLength, int maxLength, string text, bool hide)
        {
            ConsoleKeyInfo key = new ConsoleKeyInfo();
            Console.CursorVisible = true;

            while (true)
            {
                key = Console.ReadKey(true);

                if (key.Key == ConsoleKey.Escape)
                {
                    Console.WriteLine();
                    Console.CursorVisible = false;
                    return null;
                }
                else if (key.Key == ConsoleKey.Enter)
                {
                    if (text.Length >= minLength)
                    {
                        Console.WriteLine();
                        Console.CursorVisible = false;
                        return text;
                    }
                }
                
                if (key.Key != ConsoleKey.Backspace && text.Length < maxLength)
                {
                    text += key.KeyChar;
                    if (hide)
                        Console.Write("*");
                    else
                        Console.Write(key.KeyChar);
                }
                else if (key.Key == ConsoleKey.Backspace && text.Length > 0)
                {
                    text = text.Substring(0, text.Length - 1);
                    Console.Write("\b \b");
                }
            }
        }

        public static string Text(int minLength, int maxLength, string text)
        {
            ConsoleKeyInfo key = new ConsoleKeyInfo();
            Console.CursorVisible = true;

            while (true)
            {
                key = Console.ReadKey(true);

                if (key.Key == ConsoleKey.Escape)
                {
                    Console.WriteLine();
                    Console.CursorVisible = false;
                    return null;
                }
                else if (key.Key == ConsoleKey.F12)
                {
                    if (text.Length >= minLength)
                    {
                        Console.WriteLine();
                        Console.CursorVisible = false;
                        return text;
                    }
                }

                if (key.Key != ConsoleKey.Backspace && text.Length < maxLength)
                {
                    if (key.Key == ConsoleKey.Enter)
                    {
                        text += Environment.NewLine;
                        Console.WriteLine();
                    }
                    else
                    {
                        text += key.KeyChar;
                        Console.Write(key.KeyChar);
                    }   
                }
                else if (key.Key == ConsoleKey.Backspace && text.Length > 0)
                {
                    text = text.Substring(0, text.Length - 1);
                    Console.Write("\b \b");
                }
            }
        }
    }
}
