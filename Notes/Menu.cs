﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Notes
{
    class Menu
    {
        public int itemsPerPage;

        private string header;
        private string footer;
        private List<List<string>> pages;

        public Menu()
        {
            pages = new List<List<string>>();
            pages.Add(new List<string>());
            header = "";
            footer = "";
            itemsPerPage = 10;
        }

        public Menu(string header, string footer)
        {
            pages = new List<List<string>>();
            pages.Add(new List<string>());
            this.header = header;
            this.footer = footer;
            itemsPerPage = 10;
        }

        public Menu(string header, string footer, int pagesCount, int itemsPerPage)
        {
            pages = new List<List<string>>();
            for (int i = 0; i < pagesCount; i++)
                pages.Add(new List<string>());

            this.header = header;
            this.footer = footer;
            this.itemsPerPage = itemsPerPage;
        }

        public void AddOption(string option)
        {
            pages[0].Add(option);
        }

        public void AddOption(string option, int page)
        {
            pages[page - 1].Add(option);
        }

        public void ShowMenu(int selected, out int input)
        {
            Console.Clear();
            ConsoleKeyInfo key = new ConsoleKeyInfo();
            int page = 1;

            while (true)
            {
                Console.WriteLine(header);
                selected = MoveSelection(key.Key, selected, page, out page);

                foreach (string option in pages[0])
                {
                    if (selected == pages[0].IndexOf(option))
                    {
                        Console.ForegroundColor = ConsoleColor.White;
                        Console.WriteLine(" - " + option + " - ");
                    }
                    else
                    {
                        Console.WriteLine("   " + option);
                    }

                    Console.ForegroundColor = ConsoleColor.Gray;
                }
                Console.WriteLine(footer);

                key = Console.ReadKey(true);
                // cancel
                if (key.Key == ConsoleKey.Escape)
                {
                    input = -1;
                    return;
                }
                else if (key.Key == ConsoleKey.Enter)
                {
                    break;
                }
                Console.Clear();
            }
            
            input = selected;
        }

        private int MoveSelection(ConsoleKey input, int selected, int page, out int pageNr)
        {
            if (input == ConsoleKey.W || input == ConsoleKey.UpArrow)
            {
                pageNr = page;

                if (selected > 0)
                    return selected - 1;
                else
                    return selected;
            }
            else if (input == ConsoleKey.S || input == ConsoleKey.DownArrow)
            {
                pageNr = page;

                if (selected < (pages[page - 1].Count - 1))
                    return selected + 1;
                else
                    return selected;
            }
            else if ((input == ConsoleKey.D || input == ConsoleKey.RightArrow))
            {
                if (page < pages.Count)
                {
                    pageNr = page + 1;
                    return 0;
                }
                else
                {
                    pageNr = page;
                    return selected;
                }
            }
            else if ((input == ConsoleKey.A || input == ConsoleKey.LeftArrow))
            {
                if (page > 1)
                {
                    pageNr = page - 1;
                    return 0;
                }
                else
                {
                    pageNr = page;
                    return selected;
                }
            }
            else
            {
                pageNr = page;
                return selected;
            }
        }

        public int ShowMenuWithPages(int selected, out int input)
        {
            Console.Clear();
            ConsoleKeyInfo key = new ConsoleKeyInfo();
            int page = 1;

            while (true)
            {
                Console.WriteLine(header);
                selected = MoveSelection(key.Key, selected, page, out page);

                foreach (string option in pages[page - 1])
                {
                    if (selected == pages[page - 1].IndexOf(option))
                    {
                        Console.ForegroundColor = ConsoleColor.White;
                        Console.WriteLine(" - " + option + " - ");
                    }
                    else
                    {
                        Console.WriteLine("   " + option);
                    }

                    Console.ForegroundColor = ConsoleColor.Gray;
                }
                for (int i = itemsPerPage; i > pages[page - 1].Count; i--)
                {
                    Console.WriteLine();
                }
                Console.WriteLine("\n\t| strona " + page + " / " + pages.Count + " | ");

                Console.WriteLine(footer);

                key = Console.ReadKey(true);
                // cancel
                if (key.Key == ConsoleKey.Escape)
                {
                    input = -1;
                    return 0;
                }
                else if (key.Key == ConsoleKey.Enter)
                {
                    break;
                }
                Console.Clear();
            }

            input = selected;
            return page;
        }
    }
}
