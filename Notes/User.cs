﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Notes
{
    [Serializable]
    class User
    {
        public string username;
        public string password;
        [NonSerialized]
        public bool logged = false;

        public User()
        {

        }

        public User(string username, string password)
        {
            this.username = username;
            this.password = password;
        }

        public bool LogIn()
        {
            Console.Clear();
            Console.WriteLine("--- Logowanie ---\n");
            Console.Write("Podaj login: ");
            username = Input.TextLine(0, 50, string.Empty, false);
            if (username == null)
            {
                return false;
            }

            Console.Write("Podaj hasło: ");
            password = Input.TextLine(0, 50, string.Empty, true);
            if (password == null)
            {
                return false;
            }

            User findUser = DataManager.instance.FindUser(username);

            if (findUser == null)
            {
                Console.WriteLine("Niepoprawne dane!");
                Console.ReadKey();
                return false;
            }
            else
            {
                if (findUser.password != password)
                {
                    Console.WriteLine("Niepoprawne dane!");
                    Console.ReadKey();
                    return false;
                }

                Console.Clear();
                Console.WriteLine("\n\tWitaj " + username + "!");
                logged = true;
                return true;
            }
        }

        public bool Register()
        {
            Console.Clear();
            Console.WriteLine("--- Rejestracja ---\n");
            Console.Write("Podaj login: ");
            username = Input.TextLine(1, 50, string.Empty, false);
            if (username == null)
            {
                return false;
            }

            Console.Write("Podaj hasło (min. 4 znaki): ");
            password = Input.TextLine(4, 50, string.Empty, false);
            if (password == null)
            {
                return false;
            }

            Console.Write("Powtórz hasło: ");
            string pass = Input.TextLine(4, 50, string.Empty, false);
            if (pass == null)
            {
                return false;
            }

            if (password != pass)
            {
                Console.WriteLine("Hasła nie są takie same!");
                Console.ReadKey();
                return false;
            }
            else
            {
                if (DataManager.instance.UserExists(username))
                {
                    Console.WriteLine("Istnieje już użytkownik o podanym loginie!"); ;
                    Console.ReadKey();
                    return false;
                }
                else
                {
                    // create new user
                    logged = true;
                    DataManager.instance.users.Add(new User(username, password));
                    DataManager.instance.SaveUsers();

                    Console.Clear();
                    Console.WriteLine("Rejestracja udana");
                    Console.ReadKey();
                    return true;
                }
            }
        }

        public void LogOut()
        {
            logged = false;
            //Console.Clear();
            //Console.WriteLine("Wylogowano poprawnie");
            //Console.ReadKey();
        }

        public void CreateNote()
        {
            Console.Clear();
            Note note = new Note();
            Console.WriteLine("--- Tworzenie notatki ---\n");
            Console.WriteLine("[ESC], aby anulować");
            Console.Write("Tytuł: ");
            note.title = Input.TextLine(1, 50, string.Empty, false);
            if (note.title == null)
            {
                Console.Clear();
                Console.WriteLine("Notatka anulowana");
                Console.ReadKey();
                return;
            }

            Console.Clear();
            Console.WriteLine("--- Napisz notatkę ---\n");
            Console.WriteLine("[ESC], aby anulować, [F12], aby zapisać\n");
            note.text = Input.Text(0, 500, string.Empty);
            if (note.text == null)
            {
                Console.Clear();
                Console.WriteLine("Notatka anulowana");
                Console.ReadKey();
                return;
            }

            // save note
            note.creator = username;
            note.date = DateTime.Now;
            DataManager.instance.notes.Add(note);
            DataManager.instance.SaveNotes(this);

            Console.Clear();
            Console.WriteLine("Notatka utworzona!");
            Console.ReadKey();
        }

        public List<Note> GetNotes()
        {
            List<Note> userNotes = new List<Note>();
            foreach (Note note in DataManager.instance.notes)
            {
                if (note.creator == username)
                {
                    userNotes.Add(note);
                }
            }

            return userNotes;
        }
    }
}
